#!/bin/bash

#Crea el namespace si no existe
kubectl create namespace testing-app || true
#Crea secret para conectar con el registry
kubectl delete secret regcred -n testing-app || true
kubectl -n testing-app create secret docker-registry regcred \
        --docker-server="registry.gitlab.com" \
        --docker-username="britoangel12" \
        --docker-email="britogarciaangel123@gmail.com" \
        --docker-password=${REGISTRY_PASS} || true

# Crea el deployment y lo expone en un service
kubectl -n testing-app apply -f k8s/deployment.yaml
# Crea el autoscaler
kubectl -n testing-app apply -f k8s/autoscaler.yaml