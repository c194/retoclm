# Se define la base que sera node slim
FROM node:14-slim

# Se define el directorio donde se colocara todo el codigo dentro del contenedor
WORKDIR /app

# Se copia los package.json
COPY package*.json ./

# Se instalan las dependencias
RUN npm install

# Bundle app
COPY . .

# Expone el puerto 3000
EXPOSE 3000

# Se ejecuta la app
CMD [ "node", "index.js" ]
